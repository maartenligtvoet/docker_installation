#!/bin/bash
# simple docker install script for first run of nginx in combination with lets encrypt
# Information, this script follows: https://www.humankode.com/ssl/how-to-set-up-free-ssl-certificates-from-lets-encrypt-using-docker-and-nginx
# usage: run this script with sudo permissions: sudo ./<script> --domain mydomain.com

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Maarten Ligtvoet, Carlo van Wyk
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2018 08 06: initial version

# set variables
# assumeyes option for automated installation, default is to ask at all times
ASSUME_YES=
# location for docker nginx
docker_nginx_dir=/docker/letsencrypt-docker-nginx/src/letsencrypt/
# where is docker-compose binary located
docker_compose_binary="/usr/local/bin/docker-compose"
# for usage function
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

echo The script $0 was called as $0 $@

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # check whether variable is set
    if [[ ! -v $1 ]];
        then
           echo $0 ERROR: $1 is not set!
           echo "Either put it in the file 'settings' or see --help"
           echo ""
           usage
        else
            if [[ ${DONT_DISPLAY} ]]
                then
                    echo "    ${1} = <not showing>"
                else
                    echo "    ${1} = ${!1}"
            fi
    fi
}
# end of function variable_set

function sanity_check ()
{
    # sanity check
    # set variables
    # get common parameters from settings script
    # note that for this script all variables are passed as argument or supplied above
    . ./settings

    # these parameters needs to be set:
    echo $0 Installing with the following settings:
    variable_set domain
    variable_set email
}
# end of function sanity_check

function abort_installation ()
{
    echo Cancelling installation
    exit 2
}
# end of function abort_installation

function delete_dirs ()
{
    # stop old containers
    docker stop letsencrypt-nginx-container
    # remove old directories
    rm -Rf ${docker_nginx_dir}
    ${docker_compose_binary} down --rmi all -v --remove-orphans
}
# end of function delete_dirs

function mkdir_dirs ()
{
    # create directories
    mkdir -p ${docker_nginx_dir}letsencrypt-site
    cd ${docker_nginx_dir}
}
# end of function mkdir_dirs

function docker_compose ()
{
    # create docker compose file
    touch ${docker_nginx_dir}docker-compose.yml
    tee -a "docker-compose.yml" > /dev/null <<_EOF_
version: '3.1'

services:

  letsencrypt-nginx-container:
    container_name: 'letsencrypt-nginx-container'
    image: nginx:latest
    ports:
      - "80:80"
    volumes:
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
      - ./letsencrypt-site:/usr/share/nginx/html
    networks:
      - docker-network

networks:
  docker-network:
    driver: bridge
_EOF_
}
# end of function docker_compose

function nginx_config_file ()
{
    # create nginx file
    touch ${docker_nginx_dir}nginx.conf
    tee -a "nginx.conf" > /dev/null <<_EOF_
server {
    listen 80;
    listen [::]:80;
    server_name ${domain} www.${domain};

    location ~ /.well-known/acme-challenge {
        allow all;
        root /usr/share/nginx/html;
    }

    root /usr/share/nginx/html;
    index index.html;
}
_EOF_
}
# end of function nginx_config_file

function nginx_index_file ()
{
    # create nginx file
    touch ${docker_nginx_dir}letsencrypt-site/index.html
    tee -a "letsencrypt-site/index.html" > /dev/null <<_EOF_
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Let's Encrypt First Time Cert Issue Site</title>
</head>
<body>
    <h1>Oh, hai there on ${domain}!</h1>
    <p>
        This is the temporary site that will only be used for the very first time SSL certificates are issued by Let's Encrypt's
        certbot.
    </p>
</body>
</html>
_EOF_
}
# end of function nginx_index_file

function run_docker_nginx_container ()
{
    # Before running the Certbot command, 
    # spin up a Nginx container in Docker to ensure the temporary Nginx site is up and running
    cd ${docker_nginx_dir}
    ${docker_compose_binary} up -d
}
# end of function run_docker_nginx_container

function check_website_live ()
{
    echo Optional: Navigate to http://${domain} and check for yourself that the website displays the default 'O hai there' index page

    if [[ ${ASSUME_YES} ]]; then
       echo Continuing
    else
       read -p "Continue (y/n)? " choice
       case "$choice" in
         y|Y ) echo Continuing;;
         n|N ) abort_installation;;
         * ) abort_installation;;
       esac
    fi
}
# end of function check_website_live

function issue_cert ()
{
    # arguments passed to this function can toggle staging or production

    # The command does the following:
    # Run docker in interactive mode so that the output is visible in terminal
    # If the process is finished close, stop and remove the container
    # Map 4 volumes from the server to the Certbot Docker Container:
    # - The Let's Encrypt Folder where the certificates will be saved
    # - Lib folder
    # - Map our html and other pages in our site folder to the data folder that let's encrypt will use for challenges.
    # - Map a logging path for possible troubleshooting if needed
    # For staging, we're not specifying an email address
    # We agree to terms of service
    # Specify the webroot path
    # Issue the certificate to be valid for the A record and the CNAME record
    # 
    # for testruns/staging:
    # --staging: Run as staging
    # --register-unsafely-without-email: For staging, we're not specifying an email address
    # 
    # for production runs:
    # --email youremail@domain.com: Which email to register when requesting certificate
    # --no-eff-email: Don't send information emails

    docker run -it --rm \
    -v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
    -v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
    -v ${docker_nginx_dir}letsencrypt-site:/data/letsencrypt \
    -v "/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
    certbot/certbot \
    certonly --webroot \
     --agree-tos \
    --webroot-path=/data/letsencrypt \
    -d ${domain} -d www.${domain} $@
}
# end of function issue_cert

function cert_get_info ()
{
    echo Display info for issued certificate
    docker run --rm -it --name certbot \
    -v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
    -v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
    -v ${docker_nginx_dir}letsencrypt-site:/data/letsencrypt \
    certbot/certbot \
    certificates $@
}
# end of function cert_get_info

function cleanup_staging ()
{
    echo Cleanup staging artefacts
    rm -rf /docker-volumes/
}
# end of function cleanup_staging

function docker_down ()
{
    cd ${docker_nginx_dir}
    ${docker_compose_binary} down
}
# end of function docker_down

function usage ()
{
    echo "Usage: Usage: ${SCRIPT} --domain MY.DOMAIN.COM
"
        echo "
        Options:
        -y | --yes                     Automated installation: assume yes and continue with default options
        -d | --domain                  Which domain to issue a SSL certificate for with lets encrypt
        -e | --email                   Which email to register when requesting certificate with lets encrypt
        -h | --help )                  Display this usage function
        "
    exit 1
}
# end of function usage

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -y | --yes )            ASSUME_YES=--assumeyes
                                ;;
        -d | --domain )         shift
                                domain=$1
                                ;;
        -e | --email )          shift
                                email=$1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done

if [[ ${ASSUME_YES} ]]; then
   echo Continuing
else
   echo Starting the Docker installation for nginx and lets encrypt first run
   echo CAUTION: this script will remove previous config files for this container
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_installation;;
     * ) abort_installation;;
   esac
fi

sanity_check

# if directory for this domain/certificate already exists, skip creating a certificate
certificate_domain_directory=/docker-volumes/etc/letsencrypt/live/${domain}
if [ -d "$certificate_domain_directory" ]; then 
    echo ${SCRIPT}: Warning: directory ${certificate_domain_directory} for this domain/certificate already exists, so skipping creating a certificate
else
    delete_dirs
    mkdir_dirs
    docker_compose
    nginx_config_file
    nginx_index_file
    run_docker_nginx_container
    check_website_live
    echo ${SCRIPT}: issue staging certificate
    echo ${SCRIPT}: issue_cert --register-unsafely-without-email --staging
    issue_cert --register-unsafely-without-email --staging
    cert_get_info --staging
    echo ${SCRIPT}: clean up staging certificate
    echo ${SCRIPT}: cleanup_staging
    cleanup_staging
    echo ${SCRIPT}: issue production certificate
    echo ${SCRIPT}: issue_cert --email ${email} --no-eff-email
    issue_cert --email ${email} --no-eff-email
    echo ${SCRIPT}: cert_get_info
    cert_get_info
    echo If everything ran successfully, we run a docker-compose down command to stop the temporary Nginx site
    if [[ ${ASSUME_YES} ]]; then
       echo Continuing
    else
       read -p "Continue (y/n)? " choice
       case "$choice" in
         y|Y ) echo Continuing;;
         n|N ) abort_installation;;
         * ) abort_installation;;
       esac
    fi
    docker_down
fi

echo Continue with: Set up Your Production Site to Run in a Nginx Docker Container
echo Run install_docker_nginx_productionsite.sh --help for options

echo END of docker installation for nginx and lets encrypt first run
# EOF