#!/bin/bash
# Create self signed certificate with openssl, CentOS 7
# Usage: run this script with sudo permissions: sudo ./<script>

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2017 05 08: initial version
# 2018 08 07: version for docker

# variables
NGINX_LOCATION_DOMAINS=/etc/nginx/
NGINX_CONF=/etc/nginx/nginx.conf
# get this scriptname
SCRIPT_NAME=$(basename $BASH_SOURCE)
# default
CERTIFICATE_DIRECTORY=/etc/nginx/ssl

echo The script $0 was called as $0 $@

function yum_prerequisites ()
{
    # Step 1 — Install the Certbot Let's Encrypt Client
    # Enable access to the EPEL repository on your server by typing:
    echo nothing needed for yum, openssl?

# end of function yum_prerequisites
}

function nginx_conf ()
{
    # Step 2 — Obtain a Certificate
    # we assume nginx has been installed already

    echo Create nginx domain config ${NGINX_CERTIFICATE_DOMAIN}

cat > ${NGINX_CERTIFICATE_DOMAIN} << _EOF_
server {
        listen       80;
        listen       [::]:80;
        server_name  ${DOMAIN};
        root         ${WEBROOT_PATH};

    location /.well-known/acme-challenge {
        root ${WEBROOT_PATH};
    }
    location / {
        return 301 https://${DOMAIN}$request_uri;
    } 
}
_EOF_

    # activate that domain
    cd ${NGINX_LOCATION_DOMAINS}/sites-enabled
    ln -s ${NGINX_CERTIFICATE_DOMAIN}

    # TODO: check if file exists, cert_domain and nginx_conf

    # Check the nginx configuration for syntax errors 
    # if no errors: reload nginx config
    nginx -t -c /etc/nginx/nginx.conf && systemctl reload nginx

# end of function nginx_conf
}

function create_certificate ()
{
    # TODO: output should be in PEM
    # fullchain.pem:/etc/letsencrypt/live/${domain}/fullchain.pem
    #   - /docker-volumes/etc/letsencrypt/live/${domain}/privkey.pem

    mkdir -p ${CERTIFICATE_DIRECTORY}/fullchain.pem
    mkdir -p ${CERTIFICATE_DIRECTORY}/privkey.pem
    cd ${CERTIFICATE_DIRECTORY}
    DOMAIN=$1

    # create password for openssl passphrase
    newpwd=`tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1`
    touch passphrase.txt
    tee -a "passphrase.txt" > /dev/null <<_EOF_
${newpwd}
_EOF_
    
    openssl genrsa -passout file:passphrase.txt -des3 -out server.key 1024 
    # Enter pass phrase for server.key:
    #  (secret: 1234)
    # maybe use: -pass pass:somepassword
    # TODO: moet hier nog een pass bij?
    echo     openssl req -new -key server.key -out server.csr -subj '/CN='"${DOMAIN}"'/O=My ART-DECOR Instance/C=NL'
    openssl req -passin file:passphrase.txt -new -key server.key -out server.csr -subj '/CN='"${DOMAIN}"'/O=My ART-DECOR Instance/C=NL'
    # Enter pass phrase for server.key:
    # A challenge password []: (leeg laten)
    cp server.key server.key.org
    echo openssl rsa -passin file:passphrase.txt -in server.key.org -out server.key
    openssl rsa -passin file:passphrase.txt -in server.key.org -out server.key 
    #     (pass phrase: 1234)
    #  writing RSA key
    openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
    rm server.key.org server.csr
    # certificaat: server.crt  
    # private key: server.key
    # convert to pem format
    echo convert to pem format
    # openssl x509 -inform DER -outform PEM -in server.crt -out fullchain.pem
    mv server.crt fullchain.pem/fullchain.pem
    openssl rsa -in server.key -text > privkey.pem/privkey.pem

    # secure the permissions for the private key
    chown nginx:nginx *
    chmod 600 *.key
    
# end of function create_certificate
}

function usage ()
{
    echo "Usage: ${SCRIPT_NAME}"
        echo "
        Options:
        -d | --domain )         What domain to create a self-signed certifcate for with openssl
        --directory )           Which directory to write certificate to
        --skipyum )             Skip yum prerequisites
        --skipnginx )           Leave nginx config intact. Don't mess with it.
        -h | --help )           Display this usage function
        "
    exit 1
}
# end of function usage

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -d | --domain )         shift
                                DOMAIN=$1
                                ;;
        --directory )           shift
                                CERTIFICATE_DIRECTORY=$1
                                ;;
        --skipyum )             SKIPYUM=1
                                ;;
        --skipnginx )           SKIPNGINX=1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done
 
# sanity check
if [ -z ${DOMAIN+x} ]; then
   echo "Domain is missing, please pass a domain to request a certificate for, with --domain <DOMAIN>"
   exit
fi
# set path for nginx domain configuration file
NGINX_CERTIFICATE_DOMAIN=${NGINX_LOCATION_DOMAINS}sites-available/${DOMAIN}
# Check if nginx configfile is already present, if so abort
! [ -f "${NGINX_CERTIFICATE_DOMAIN}" ] || { echo "ERROR: File ${NGINX_CERTIFICATE_DOMAIN} already present" ; exit 1 ;}

# If SKIPYUM is 1, then skip yum config
if [[ "${SKIPYUM}" == "1" ]]; then
        echo ${SCRIPT_NAME}: Skipping yum config
    else
        yum_prerequisites
fi

# If SKIPNGINX is 1, then skip nginx config
if [[ "${SKIPNGINX}" == "1" ]]; then
        echo ${SCRIPT_NAME}: Skipping nginx config
    else
        nginx_conf
fi

create_certificate ${DOMAIN}

echo Done!
echo You should do the following by hand:
echo Add key and cert to nginx config