#!/bin/bash
# simple exist-db install script for ART-DECOR
# usage: run this script with sudo permissions: sudo ./<script>
# to install to a different path: sudo ./<script> --path /usr/local/exist_repo
# usage: run this script to override default package with: sudo ./<script> -f <package location in ../packages> so:  sudo ./install_exist.sh -f eXist-db-setup-3.1.0.jar

# version
# 2012 08 22: initial version
# 2013 02 25: changed to more general setup
# 2013 09 28: changed to exist-db 2.1
# 2014 04 24: existdb does not work with shell nologin, so bash
# 2014 11 22: modified for exist 2.2 LTS
# 2015 10 27: remove old symlink in usr/local
# 2016 04 07: overwrite option for the location_symlink
# 2017 02 14: modified for exist 3.0
# 2017 03 16: modified for exist 3.1.0
# 2017 04 19: modified for exist 3.1.1
# 2017 04 22: added exist for automated installation
# 2017 08 03: Introduced logic: set contextpath to default, start/stop, set contextpath to /
# 2017 08 24: yum_prerequisites: install expect
# 2018 02 23: fix for system_installer
# 2018 02 23: fix for sanity check
# 2018 03 02: support non-default ports for the repo eXist-db
# 2018 09 12: adapted for docker/debian

# disabled in docker version
# get common parameters from settings script
# . ./settings
        # provides input for the following variables:
        # exist_password=test123
        # exist_maxmem=8192
        # exist_cachemem=1024
        # tooling_location -> to find the exist-db package in ./packages
        # exist_file
        # exist_version
                # jetty.http.port
                # jetty.ssl.port

# set the user to run the process as
application_user=existdb
# the location where application will be symlinked
location_symlink=/usr/local/exist_atp
# what system installer? yum / apt-get / ..
system_installer=apt-get
# set location where eXist-db installer package is located
tooling_location=/root/assets/

echo printwd
pwd
echo l s dir
ls -al

function file_present ()
{
   file=$1
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage ;}
}

function yum_prerequisites ()
{
        # for automating the eXist-db installation, we use expect
        ${system_installer} -y install expect

# end of function yum_prerequisites
}

function sanity_check ()
{
       # sanity check
       # these parameters needs to be set:

       echo $0 Installing eXist-db with the following settings:
       for var in exist_maxmem exist_cachemem exist_file exist_version; do
         if [ -z "${!var}" ]
           then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings' or see --usage"
           usage
         else echo "    ${var}=${!var}"
         fi
       done

       var=exist_password
         if [ -z "${!var}" ]; then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings' or see --usage"; usage; else echo "    ${var}=not showing ..." ; fi

       # display this setting
           echo "    location_symlink = ${location_symlink}"

       # test if settings file is present and readable
       # file_present ${tooling_location}/packages/${exist_file} ${tooling_location}/packages/${exist_file}
       file_present ${tooling_location}${exist_file} ${tooling_location}${exist_file}

# end of sanity_check
}

function setup_config ()
{
        # the location where application will be installed
        location_application=${location_symlink}_${exist_version}_$(date '+%Y%m%d')

# end of setup_config_ports
}

function install_application ()
{
        # create directory for exist
        if [ ! -d ${location_application} ]; then
          mkdir $location_application
          else
            echo $0 ERROR: folder ${location_application} already exists
            echo ""
            usage
        fi
        cd $location_application
        # install application
        echo Starting exist-db installer. Press enter if settings are correct!
        # automated installation needs 3 parameters, could be input from file settings, or passed to script
        # Examples:
        # exist_password=test123
        # exist_maxmem=8192
        # exist_cachemem=1024
        # to run manually: java -jar ${tooling_location}/packages/${exist_file}
        expect -c "
          set timeout -1
          spawn java -jar ${tooling_location}${exist_file}
          expect \"?elect target path\"
          send \"\r\"
          expect \"?redisplay\"
          send \"1\r\"
          expect \"?Data dir:\"
          send \"\r\"
          expect \"?redisplay\"
          send \"1\r\"
          expect \"?Enter password:\"
          send \"${exist_password}\r\"
          expect \"?Enter password:\"
          send \"\r\"
          expect \"?Maximum memory\"
          send \"${exist_maxmem}\r\"
          expect \"?Cache memory\"
          send \"${exist_cachemem}\r\"
          expect \"?redisplay\"
          send \"1\r\"
          expect eof"

# end of install_application
}

function symlink_application ()
{
        # remove old symlink
        rm ${location_symlink}
        # set up symlink
        ln -s ${location_application} ${location_symlink}
        echo ln -s ${location_application}/ ${location_symlink}

# end of function symlink_application
}

function setup_permissions ()
{
        # set up the user and permissions
        # adduser to run the application as
        # Normally for CentOS: adduser --shell /bin/bash ${application_user}
        # for debian in docker:
        adduser --disabled-password --gecos "" --shell /bin/bash ${application_user}

        # set up permissions
        chown -h ${application_user}:${application_users} ${location_symlink}
        chown -R ${application_user}:${application_users} ${location_application}
}

function configure_as_service ()
{
        # set up service script
        export RUN_AS_USER=existdb
        ${location_symlink}/tools/yajsw/bin/installDaemon.sh

# end of function configure_as_service
}

function etc_init ()
{
        # set up init script
        cd /etc/init.d
        # ${location_symlink##*/}: get the last part from location symlink. so /usr/local/exist_atp = exist_atp
        ln -s ${location_application}/tools/wrapper/bin/exist.sh ./${location_symlink##*/}

        # dont start exist-db on server restart, since this might render the server uncontrollable, if exist-db does not start

# end of function etc_init
}

function application_config2 ()
{
        # setup config
        echo ""
        echo Settings for exist-db:

        # set exist confidential port to $exist_port_conf
        # sed -i '/confidentialPort/s/<Set name="confidentialPort">.*/<Set name="confidentialPort">'${exist_port_conf}'<\/Set>/g' ${location_symlink}/tools/jetty/etc/jetty.xml

        # sed -i '/Set name="Port"/s/<Set name="Port">.*/<Set name="Port"><SystemProperty name="jetty.port.ssl" default="'${exist_port_conf}'"\/><\/Set>/g' ${location_symlink}/tools/jetty/etc/jetty.xml
        # echo Exist port confidential: $exist_port_conf

        # set the user to run exist as
        sed -i 's/^#RUN_AS_USER.*/RUN_AS_USER='${application_user}'/g' ${location_symlink}/tools/wrapper/bin/exist.sh

# end of function application_config2
}

function application_config ()
{
        # this is a workaround: contextPath needs to be /exist when starting the first time. Then start/stop eXist. Change contextPath afterwards to /exist
# echo ''
# echo context staat op zoals die in installer staat, op /
#     echo BEGIN Show contextPath
#     cat ${location_symlink}/tools/jetty/webapps/exist-webapp-context.xml
#     echo END Show contextPath
#
# echo symlink ${location_symlink}
#
#         # change contextpath to /exist
#         echo change contextpath to /exist
#         sed -i 's/<Set name="contextPath">\/.*<\/Set>/<Set name="contextPath">\/exist<\/Set>/g' ${location_symlink}/tools/jetty/webapps/exist-webapp-context.xml
# echo ''
#     echo BEGIN Show contextPath
#     cat ${location_symlink}/tools/jetty/webapps/exist-webapp-context.xml
#     echo END Show contextPath
#         echo Starting eXist-db
#         systemctl start eXist-db
# # sleep for 1 minute while eXist is starting
# sleep 30
# echo curl localhost:8877/exist/xmlrpc -L -X POST
# curl localhost:8877/exist/xmlrpc
#
# echo curl localhost:8877/exist/apps/
# curl localhost:8877/exist/apps/
#
#         echo Status eXist-db
#         systemctl status eXist-db
#         echo Stopping eXist-db
#         systemctl stop eXist-db
#         echo Status eXist-db
#         systemctl status eXist-db
#         # change contextpath to /
#         echo change contextpath to /
#         sed -i 's/<Set name="contextPath">\/exist<\/Set>/<Set name="contextPath">\/<\/Set>/g' ${location_symlink}/tools/jetty/webapps/exist-webapp-context.xml
# echo ''
     echo BEGIN Show contextPath
     cat ${location_symlink}/tools/jetty/webapps/exist-webapp-context.xml
     echo END Show contextPath
#         echo Starting eXist-db
#        systemctl start eXist-db
#        echo Status eXist-db
#        systemctl status eXist-db

# # test
# echo curl localhost:8877/xmlrpc -L -X POST
# curl localhost:8877/xmlrpc -L -X POST
#
# echo curl localhost:8877/apps/
# curl localhost:8877/apps/

    # (optional) set our own ports instead of default ports
        if [ ! -z "${jetty_http_port}" ]; then
          echo Setting jetty.http.port to ${jetty_http_port}
          sed -i '/jetty.http.port/s/8080/'${jetty_http_port}'/g' ${location_symlink}/tools/jetty/etc/jetty-http.xml
          # port might already be set to our port
          sed -i '/jetty.http.port/s/8877/'${jetty_http_port}'/g' ${location_symlink}/tools/jetty/etc/jetty-http.xml
    fi
        if [ ! -z "${jetty_ssl_port}" ]; then
          echo Setting jetty.ssl.port to ${jetty_ssl_port}
          sed -i '/jetty.ssl.port/s/8443/'${jetty_ssl_port}'/g' ${location_symlink}/tools/jetty/etc/jetty-ssl.xml
          # port might already be set to our port
          sed -i '/jetty.ssl.port/s/8477/'${jetty_ssl_port}'/g' ${location_symlink}/tools/jetty/etc/jetty-ssl.xml

        fi

# end of function application_config
}

function usage ()
{
        echo "Usage: $0 --exist_file <jarfile to install from ../packages>"
        echo "
        -p | --path )           Option to overwrite location_symlink: this provides an alternative name for the symlink we create. Example: --path /usr/local/exist_31_atp
        -f | --exist_file )     [required] which eXist-db .jar package to install
        --password )            [required] provide an admin password for eXist-db
        -m | --exist_maxmem )   [required] the maxmemory for eXist-db
        -c | --exist_cachemem ) [required] the cachemem for eXist-db
        --exist_version )       [required] provides the name for eXist-db in the path where we install the database
                --port_http )           [optional] set your own http port instead of default. For instance for the repo
                --port_ssl )            [optional] set your own ssl port instead of default. For instance for the repo
        -h | --help )           Display this usage function

        Please note that you can also set variables in the file 'settings'
        "
        exit 1
# end of function usage
}

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -p | --path )           shift
                                MYPATHSYMLINK=$1
                                ;;
        -f | --exist_file )     shift
                                exist_file=$1
                                ;;
        --password )            shift
                                exist_password=$1
                                ;;
        -m | --exist_maxmem )   shift
                                exist_maxmem=$1
                                ;;
        -c | --exist_cachemem ) shift
                                exist_cachemem=$1
                                ;;
        --exist_version )       shift
                                exist_version=$1
                                ;;
        --port_http )           shift
                                jetty_http_port=$1
                                ;;
        --port_ssl )            shift
                                jetty_ssl_port=$1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done

# option to overwrite location_symlink
if [ -n "${MYPATHSYMLINK}" ]; then
   location_symlink=${MYPATHSYMLINK}
fi
yum_prerequisites
sanity_check
setup_config
install_application
symlink_application
setup_permissions
configure_as_service
application_config

echo ""
echo "Installation finished. You can start it with:"
echo "   sudo systemctl start eXist-db"
echo "For Centos 6:"
echo "   sudo service ${location_symlink##*/} start"
echo Check out the status with:
echo "   sudo service ${location_symlink##*/} status"
echo "   sudo netstat -anp|grep :8"
echo "   sudo ps aux|grep java"

# EOF


