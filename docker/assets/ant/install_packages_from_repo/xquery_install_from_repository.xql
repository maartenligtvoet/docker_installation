xquery version "3.0";

import module namespace repo="http://exist-db.org/xquery/repo";
(:  install xar from repo :)
(: example = 
repo:install-and-deploy("http://art-decor.org/ns/art", "", "http://decor.nictiz.nl/apps/public-repo-dev/modules/find.xql")
:)
(: would want to use: repo:install-and-deploy(request:get-parameter("package", ""), "", request:get-parameter("repository", ""))
:)
(:  install xar from repo :)
repo:install-and-deploy(request:get-parameter("package", ""), "", "http://decor.nictiz.nl/apps/public-repo-dev/modules/find.xql")