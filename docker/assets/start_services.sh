#!/bin/sh
# script to start services: tomcat and eXist-db

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Melle Sieswerda, Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# set variables
domain=domainplaceholder

echo The script $0 was called as $0 $@

# main logic

service tomcat7 start

echo "Starting eXist-db"
# sudo -u existdb /usr/local/exist_atp_2.2/tools/wrapper/bin/exist.sh console
# sudo -u existdb /etc/init.d/exist start
sudo -u existdb /etc/init.d/eXist-db start

TOMCAT_HOME=/var/lib/tomcat7/
# by this time the tomcat war will be deployed
# change the domain localhost to the hosted domain
# check if file is present
if [ -f "$TOMCAT_HOME/webapps/art-decor/WEB-INF/resources/config/properties-local.xml" ]; then
  # escape all forward slashes, so / becomes \/
  # domain=${domain//\//\\/}
  echo ${domain}
  sed -i '/external/s/http:\/\/localhost:8877/https:\/\/'"${domain}"'/g' $TOMCAT_HOME/webapps/art-decor/WEB-INF/resources/config/properties-local.xml
else
  echo ${SCRIPT}: File not found: $TOMCAT_HOME/webapps/art-decor/WEB-INF/resources/config/properties-local.xml
fi

/bin/bash -i
