#!/bin/sh
# Usage: build.sh --download_location_exist http://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar --art_packages "ART-1.8.61.xar DECOR-core-1.8.46.xar DECOR-services-1.8.35.xar ART-DECOR-system-services-1.8.23.xar terminology-1.8.37.xar" --download_location_orbeon http://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war
# * downloads the ART-DECOR Orbeon source, [eXistdb](http://www.exist-db.org) and eXistdb packages from the Nictiz repository
# * runs the `docker build` command

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Melle Sieswerda, Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2018 08 08: initial version
# 2018 09 04: pass art_packages as local variable
# 2018 09 05: pass exist and tomcat memory settings
# 2018 09 12: pass exist_installer_package

# set variables
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
# variable 'repository' is set as a local variable in install_docker_art-decor.sh, so is not passed to this script as argument

echo The script $0 was called as $0 $@

function abort_installation ()
{
    echo Cancelling installation
    exit 0

# end of function abort_installation
}

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # check whether variable is set
    if [[ ! -v $1 ]];
        then
           echo $0 ERROR: $1 is not set!
           echo "Either put it in the file 'settings' or see --help"
           echo ""
           usage
        else
            if [[ ${DONT_DISPLAY} ]]
                then
                    echo "    ${1} = <not showing>"
                else
                    echo "    ${1} = ${!1}"
            fi
    fi
}
# end of function variable_set

function sanity_check ()
{
    # sanity check
    # set variables

    # these parameters needs to be set:
    echo $0 Installing with the following settings:
    variable_set download_location_exist
    variable_set download_location_orbeon
    variable_set art_packages
    variable_set adminpassword --DONT_DISPLAY
    variable_set exist_maxmem
    variable_set exist_cachemem
    variable_set tomcat_maxmem
    variable_set domain
}
# end of function sanity_check

function usage ()
{
    echo "Usage: build.sh --download_location_exist http://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar --art_packages "ART-1.8.61.xar DECOR-core-1.8.46.xar DECOR-services-1.8.35.xar ART-DECOR-system-services-1.8.23.xar terminology-1.8.37.xar" --download_location_orbeon http://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war
"
        echo "
        Options:
        --download_location_exist      Location where to download the eXist-db ART-DECOR installer.
        --art_packages                 What ART-DECOR packages to get from repository and install.
        -p | --adminpassword)          [required] provide an admin password for eXist-db
        -m | --exist_maxmem )          [required] the maxmemory for eXist-db
        -c | --exist_cachemem )        [required] the cachemem for eXist-db
        -t | --tomcat_maxmem )         [required] the maxmemory for tomcat. Otherwise use default
        -d | --domain                  [required] Which domain to point tomcat to
        --download_location_orbeon     Location where to download the ART-DECOR Orbeon image.
        -h | --help )                  Display this usage function
        "
    exit 1
}
# end of function usage

# main logic

# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -ex | --download_location_exist )    shift
                                            download_location_exist=$1
                                            ;;
        -o | --download_location_orbeon )   shift
                                            download_location_orbeon=$1
                                            ;;
        -p | --adminpassword )              shift
                                            adminpassword=$1
                                            ;;
        -m | --exist_maxmem )               shift
                                            exist_maxmem=$1
                                            ;;
        -c | --exist_cachemem )             shift
                                            exist_cachemem=$1
                                            ;;
        -t | --tomcat_maxmem )              shift
                                            tomcat_maxmem=$1
                                            ;;
        -d | --domain )                     shift
                                            domain=$1
                                            ;;
        -h | --help )                       usage
                                            ;;
        * )                                 usage
    esac
    shift
  done

# first cd to script directory
cd ${SCRIPTPATH}

sanity_check

# Cache eXist-db locally
if [ ! -f ./assets/eXist-db-*.jar ]; then
    echo "Downloading eXist-db installer .jar"
    wget -P ./assets ${download_location_exist}
    # if this fails, display error
    status=$?
    if test $status -eq 0
    then
        echo "Download finished: ${download_location_exist}"
    else
        echo "ERROR: Download failed: ${download_location_exist}"
        read -p "Continue anyway (y/n)? " choice
          case "$choice" in
          y|Y ) echo Continuing;;
          n|N ) abort_installation;;
          * ) abort_installation;;
          esac
    fi
    #    wget -P ./assets http://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar
    else 
        echo "Not downloading package ./assets/eXist-db-*.jar"
        # echo "pwd, ls"
        # pwd
        # ls ./assets/eXist-db-*.jar
fi
exist_installer_package=${download_location_exist##*/}
echo Package = ${exist_installer_package}

# if the user supplied a different repository, use that as BASE_URL
if [[ ${repository} ]]; then
    echo User supplied a different repository
    BASE_URL=${repository}/public/
else
  echo Using the default repository
  # Base URL for downloading exist packages with ART-DECOR
  BASE_URL="http://decor.nictiz.nl/apps/public-repo/public/"
fi

echo ART-DECOR .xar packages that will be installed are: ${art_packages}
for i in ${art_packages}
do
    if [ ! -f "./assets/exist-packages/$i" ]; then
        echo "Downloading package $i"
        echo wget -P ./assets/exist-packages $BASE_URL$i
        wget -P ./assets/exist-packages $BASE_URL$i

        # if this fails, display error
        status=$?
        if test $status -eq 0
          then
            echo "Download finished: $BASE_URL$i"
        else
            echo "ERROR: Download failed: $BASE_URL$i"
            read -p "Continue anyway (y/n)? " choice
              case "$choice" in
              y|Y ) echo Continuing;;
              n|N ) abort_installation;;
              * ) abort_installation;;
              esac
        fi
    else 
        echo "Not downloading package $i"
        # echo "pwd, ls"
        # pwd
        # ls ./assets/exist-packages/
    fi
done

# Cache art-decor locally
if [ ! -f ./assets/art-decor.war ]; then
    echo "Downloading ART-DECOR Orbeon .war"
    wget -P ./assets ${download_location_orbeon}
    
    # if this fails, display error
    status=$?
    if test $status -eq 0
      then
      echo "Download finished: ${download_location_orbeon}"
    else
      echo "ERROR: Download failed: ${download_location_orbeon}"
      read -p "Continue anyway (y/n)? " choice
      case "$choice" in
        y|Y ) echo Continuing;;
        n|N ) abort_installation;;
        * ) abort_installation;;
      esac
    fi
    
    # wget -P ./assets http://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war
    else
        echo "Not downloading package ./assets/art-decor.war"
        # echo "pwd, ls"
        # pwd
        # ls ./assets/
        
fi

# Build the docker!
docker build -t art-decor-base -f Dockerfile.base .
# note that domain is unused, but is set directory to tomcat from install_docker_art-decor.sh to start_services.sh
docker build -t art-decor --build-arg adminpassword=${adminpassword} --build-arg exist_maxmem=${exist_maxmem} --build-arg exist_cachemem=${exist_cachemem} --build-arg tomcat_maxmem=${tomcat_maxmem} --build-arg exist_installer_package=${exist_installer_package} .