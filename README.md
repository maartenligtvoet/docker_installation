# ART-DECOR docker installer

## Purpose
ART-DECOR installation scripts that automate ART-DECOR installation.
Includes:

1. Tomcat/Orbeon
1. eXist-db database
1. NGINX (reverse-proxy) with:
    1. Let's encrypt certificates for online deployment, or
    1. self-signed certificates generated with OpenSSL for local deployments (virtualized/testing)

Contains Dockerfile and assets to create an image of ART-DECOR (www.art-decor.org). The shell scripts are written in/for Bash. However, they should be easy to port to a Windows batch file.

## Contact
support@art-decor.org

## Disclaimer
### Status
Please note that these installation scripts are currently in status: development.
Use them on testing environments for a quick deployment of ART-DECOR.
### Warning
Please note that running these script on servers may cause loss of data, since the logic in the scripts is that it will be run on a freshly installed Linux server. Any previous docker data or other production data may be lost.
Make backups before starting.

## Getting started with installation on CentOS linux
* Create a directory to store scripts in:

```
sudo mkdir /usr/local/docker_installation_art-decor
cd /usr/local/docker_installation_art-decor
```

* Install git:
```
sudo yum install git
sudo chown -R $USER:$USER /usr/local/docker_installation_art-decor
git clone git@gitlab.com:maartenligtvoet/docker_installation.git
cd docker_installation
```

* Change permissions for the script to executable:
```
chmod -R +x ./*.sh
```

* Execute the wrapper script (with root permissions):
```
./install_docker_art-decor.sh --adminpassword <PASSWORD> --location /usr/local/docker/art-decor/ --domain <DOMAIN>
```

## Documentation of scripts
### install_docker_art-decor.sh
* functions as a wrapper script to start all other scripts
* runs all other installation scripts

### install_docker.sh
* downloads docker and installs it

### build.sh
* downloads the ART-DECOR Orbeon source, [eXistdb](http://www.exist-db.org) and eXistdb packages from the Nictiz repository
* runs the `docker build` command

### run.sh
* runs the image/container with `docker run` attaching ports `8877` and `8080`

After calling `run.sh` it should be possible to:
* Logon to the eXist dashboard at [http://localhost:8877](http://localhost:8877/apps/dashboard/index.html) (use `admin/password` or whatever you put in `assets/install_existdb.py`)
* Open ART-DECOR at [http://localhost:8080/art-decor](http://localhost:8080/art-decor/home)

### install_docker_nginx_firstrun.sh
* For online deployments: request let's encrypt certificate

### install_docker_nginx_productionsite.sh
* For online deployments: deploy NGINX let's encrypt certificate
* For local deployments: deploy NGINX with self-signed certificate

### install_self_signed_certificate.sh
* For local deployments: create self-signed certifcate

## Conclusion
After running these scripts for online deployments it should be possible to access ART-DECOR at: ```https://the_domain_you_provided/art-decor```

## Copyright
    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://art-decor.org/mediawiki/index.php?title=Copyright
    
    Author: Melle Sieswerda, Maarten Ligtvoet

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.

    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

