#!/bin/bash
# simple ART-DECOR install script for docker
# usage: run this script with sudo permissions: sudo ./<script> --loction <location where the art-decor application should be installed in docker>
#   options: see usage for optional parameters

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2018 07 23: Initial version for ART-DECOR docker install script
# 2018 08 28: Add parameters for art_packages and download_location_orbeon, download_location_exist
# 2018 09 04: pass art_packages as local variable
# 2018 09 05: pass exist and tomcat memory settings
# 2018 09 07: works with self-signed certificate on: http://localhost:8080/art-decor/
# 2018 09 11: Changed variable_set
# 2018 09 15: Testing with eXist-db 4

# set variables
# get common parameters from settings script -> in sanity_check
  # see usage for optional/required parameters

# set temp directory
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
# set default application_location
application_location=/usr/local/docker/art-decor/
# set default location to download eXist-db. Override with --download_location_exist
download_location_exist=http://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar
# set default packages to install. Override with --art_packages
art_packages="ART-1.8.62.xar DECOR-core-1.8.47.xar DECOR-services-1.8.36.xar ART-DECOR-system-services-1.8.24.xar terminology-1.8.38.xar DECOR-examples-1.8.22.xar xis-1.8.32.xar"
# set default download location for orbeon. Override with --download_location_orbeon
download_location_orbeon=http://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war
# set default value
exist_maxmem=1024
exist_cachemem=128
tomcat_maxmem=1024

function file_present ()
{
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage ;}
}
# end of function file_present

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    local v="$1"
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # test if variable is set
    if [[ ! ${!v} && ${!v-unset} ]]; then
      echo $0 ERROR: $1 is not set!
      echo "Either put it in the file 'settings' or see --help"
      echo ""
      usage
    else
      if [[ ${DONT_DISPLAY} ]]
        then
          echo "    ${1} = <not showing>"
      else
        echo "    ${1} = ${!1}"
      fi
    fi
}
# end of function variable_set

function sanity_check ()
{
    # sanity check
    file_present settings
    # file_present install_docker_vonk.password

    # set variables
    # get common parameters from settings script
    . ./settings
      # see usage for optional/required parameters

    # these parameters needs to be set:
    echo $0 Installing with the following settings:
    # variable_set application_port
    # these parameters needs to be set:
    variable_set domain
    variable_set adminpassword --DONT_DISPLAY
    variable_set exist_maxmem
    variable_set exist_cachemem
    variable_set tomcat_maxmem
}
# end of function sanity_check

function set_permissions ()
{
    # set permissions for all scripts to executable
    find . -name '*.sh' -type f | xargs chmod +x
}

function abort_installation ()
{
    echo Cancelling installation
    exit 2
}
# end of function abort_installation

function config_repository ()
{
    # change the repository location where to install ART-DECOR packages from
    echo Changing default repository to ${repository}
    # escape all forward slashes, so / becomes \/
    xmlrepository=${repository//\//\\/}
    # enclose the variable with xml element
    xmlrepository="<repository>${xmlrepository}<\/repository>"
    sed -i 's/^[ ]*<repository>.*/'"${xmlrepository}"'/g' ./docker/assets/configuration.xml
}
# end of function config_repository

function install_docker ()
{
    echo Run the docker installation script
    ./install_docker.sh $*
        # store exit status
    # normal operation: will return 0 exit stauts
    # when errors occur: will return a nonzero exit status
    status=$?

    if test $status -eq 0
      then
         echo "Script install_docker.sh finished."
      else
            echo "ERROR: Script install_docker.sh exited abnormally."
        read -p "Continue anyway (y/n)? " choice
          case "$choice" in
          y|Y ) echo Continuing;;
          n|N ) abort_installation;;
          * ) abort_installation;;
        esac
    fi
}
# end of function install_docker

function create_directory ()
{
    echo Starting the application installation on docker
    # set up directories to store the license file in
    mkdir /usr/local/docker
    cd /usr/local/docker
    rm -Rf ${application_location}
    mkdir ${application_location}
    cd ${application_location}
}
# end of function create_directory

function config_domain ()
{
    # first cd to script directory
    cd ${SCRIPTPATH}
    # replace domain=domainplaceholder with domain=<DOMAIN>
    sed -i 's/domainplaceholder/'"${domain}"'/' docker/assets/start_services.sh
}
# end of function call_build

function call_build ()
{
    # first cd to script directory
    cd ${SCRIPTPATH}
    # example:  ./docker/build.sh --adminpassword ${adminpassword} --download_location_exist http://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar --download_location_orbeon http://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war --exist_maxmem 1024 -exist_cachemem 128
    ./docker/build.sh --adminpassword ${adminpassword} --download_location_exist ${download_location_exist} --download_location_orbeon ${download_location_orbeon} --exist_maxmem ${exist_maxmem} --exist_cachemem ${exist_cachemem} --tomcat_maxmem ${tomcat_maxmem} --domain ${domain}

}
# end of function call_build

function call_run ()
{
    # first cd to script directory
    cd ${SCRIPTPATH}
    ./docker/run.sh
}
# end of function call_run

function usage_echo ()
{
echo " This script will:"
echo "   - install docker (specific docker version) (install_docker.sh)"
echo "   - start docker (install_docker.sh)"
echo "   - run the hello world example (install_docker.sh)"
echo "       Expected output:"
echo "            Hello from Docker!"
echo "            This message shows that your installation appears to be working correctly."
echo "   - install ART-DECOR (build.sh and run.sh)"
echo "       - install eXist-db"
echo "       - install Orbeon"
echo "   - install nginx with either:"
echo "       - let's encrypt live production certificate, or"
echo "       - self-signed certificate with openssl for standalone deploys"
}
# end of function usage_echo

function usage ()
{
    echo "Usage: sudo $0"
        echo "
        Please note that for now we focus on supporting CentOS linux version 7 installation.
        Options:
        -ex| --download_location_exist  [optional] Where to download eXist-db if you want to override the default
        -r | --repository               [optional] Which repository to use to pull installation packages from, if not using the default. Usage --repository \"http://decor.nictiz.nl/apps/public-repo-dev\"
        -a | --art_packages             [optional] Which ART-DECOR packages to install if not using the default. Usage --art_packages \"ART-1.8.62.xar DECOR-core-1.8.47.xar\"
        -o | --download_location_orbeon [optional] Where to download ART-DECOR orbeon from if you dont use the default.
        -l | --location                 Location where the art-decor application should be installed in docker.
        -d | --domain                   [required] Which domain to use with nginx SSL certificate
        -e | --email                    Which email to register when requesting certificate with lets encrypt
        -s | --skipdocker               Skip docker reinstallation
        -p | --adminpassword            [required] Pass the eXist-db admin password
        -m | --exist_maxmem             [optional] The maxmemory for eXist-db. Otherwise use default value
        -c | --exist_cachemem           [optional] The cachemem for eXist-db. Otherwise use default value
        -t | --tomcat_maxmem )          [optional] the maxmemory for tomcat. Otherwise use default value
        -se|-selfsignedcert             If you do not use DNS: Do not use let's encypt to issue certificate, but use standalone openssl for self-signed certificate. 
                                        Note that using a self-signed certificate is not secure!
        -y | --yes )                    Option to automate installation: assumeyes for installation/deletion with yum. Also skips script continuation checks.
        -h | --help )                   Display this usage function

        Please note that you must set required variables in the file 'settings'
            - (required) location
            - (optional) docker_version (if not present, the install_docker.sh script will ask for it and store it to settings)
        "
    exit 1
}
# end of function usage

# main logic
echo "Starting the application installation-script for docker."
usage_echo

# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -ex | --download_location_exist )   shift
                                            download_location_exist=$1
                                            ;;
        -r | --repository )                 shift
                                            repository=$1
                                            ;;
        -a | --art_packages )               shift
                                            art_packages=$1
                                            ;;
        -o | --download_location_orbeon )   shift
                                            download_location_orbeon=$1
                                            ;;
        -l | --location )                   shift
                                            application_location=$1
                                            ;;
        -d | --domain )                     shift
                                            domain=$1
                                            ;;
        -e | --email )                      shift
                                            email=$1
                                            ;;
        -s | --skipdocker )                 skip_docker=--skip_docker
                                            ;;
        -p | --adminpassword )              shift
                                            adminpassword=$1
                                            ;;
        -m | --exist_maxmem )               shift
                                            exist_maxmem=$1
                                            ;;
        -c | --exist_cachemem )             shift
                                            exist_cachemem=$1
                                            ;;
        -t | --tomcat_maxmem )              shift
                                            tomcat_maxmem=$1
                                            ;;
        -se | --selfsignedcert )            selfsignedcert=1
                                            ;;
        -y | --yes )                        ASSUME_YES=--assumeyes
                                            ;;
        -h | --help )                       usage
                                            ;;
        * )                                 usage
    esac
    shift
  done

sanity_check
# exporting this variable for later usage in docker/build.sh
export art_packages=${art_packages}
set_permissions

# if the user supplied a different repository, save that to a local file
if [[ ${repository} ]]; then
    export repository=${repository}
    echo config_repository
    config_repository
    # exporting this variable for later usage in docker/build.sh
else
  echo Using the default repository
fi

# run the docker installation script
install_docker_params="--location ${application_location} --p 8877"
if [[ ${skip_docker} ]]; then
    echo Skipping docker reinstallation
else
    if [[ ${ASSUME_YES} ]]; then
       echo install_docker --skipdockertest --yes ${install_docker_params}
       echo Also skipping docker hello world test
       install_docker --skipdockertest --yes ${install_docker_params}
    else
       read -p "Continue (y/n)? " choice
           case "$choice" in
               y|Y ) echo Continuing;;
               n|N ) abort_installation;;
               * ) abort_installation;;
           esac
       echo install_docker ${install_docker_params}
       install_docker ${install_docker_params}
    fi
fi
echo Installing ART-DECOR in docker
create_directory
config_domain
call_build
call_run

echo TODO ISRUNNING: install_docker_art-decor.sh
echo curl localhost:8877/rest/db/apps/art
curl localhost:8877/rest/db/apps/art
# run post_install hacks to start eXist-db
echo docker exec -it art-decor /root/install_art_decor.sh --adminpassword ${adminpassword} --post_install
exist_installer_package=${download_location_exist##*/}
docker exec -it art-decor /root/install_art_decor.sh --adminpassword ${adminpassword} --post_install --exist_installer_package ${exist_installer_package}
echo TODO ISRUNNING: install_docker_art-decor.sh 2
echo curl localhost:8877/rest/db/apps/art
curl localhost:8877/rest/db/apps/art


echo Show docker status
docker ps -a

if [[ ${selfsignedcert} ]]; then
    echo Skipping lets encrypt installation, use openssl self-signed certificate
    echo Installing nginx in docker production run with self-signed option
    # ugly hack for self-signed certificate:
    sed -i 's/\}\/fullchain.pem;/}\/fullchain.pem\/fullchain.pem;/g' ./install_docker_nginx_productionsite.sh
    sed -i 's/\}\/privkey.pem;/\}\/privkey.pem\/privkey.pem;/g' ./install_docker_nginx_productionsite.sh
    ./install_docker_nginx_productionsite.sh --domain ${domain} --yes --selfsignedcert
else
    echo Installing nginx in docker first test run
    # check if email is set
    variable_set email
    ./install_docker_nginx_firstrun.sh --domain ${domain} --yes --email ${email}
    echo Installing nginx in docker production run for use with lets encrypt
    ./install_docker_nginx_productionsite.sh --domain ${domain} --yes
fi

echo TODO ISRUNNING: install_docker_art-decor.sh 3
echo curl localhost:8877/rest/db/apps/art
curl localhost:8877/rest/db/apps/art
docker exec -it art-decor service eXist-db start


echo ${SCRIPT}: completed
# EOF